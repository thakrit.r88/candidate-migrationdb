CREATE TABLE `tbl_interview_topic_detail` (
  `id` VARCHAR(41) NOT NULL,
  `topic_id` VARCHAR(41) NOT NULL,
  `detail` VARCHAR(41) NOT NULL, 
  `created_date_time` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` VARCHAR(50) NOT NULL DEFAULT 'microservice',
  `updated_date_time` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_by` VARCHAR(50) NOT NULL DEFAULT 'microservice',
  PRIMARY KEY (`id`, `username`)
  )ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
