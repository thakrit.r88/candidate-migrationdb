CREATE TABLE `tbl_interview_topic` (
  `id` VARCHAR(41) NOT NULL,
  `username` VARCHAR(41) NOT NULL,
  `topic` VARCHAR(100) NOT NULL,
  `status` VARCHAR(20) NOT NULL DEFAULT 'Scheduled', 
  `created_date_time` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` VARCHAR(50) NOT NULL DEFAULT 'microservice',
  `updated_date_time` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_by` VARCHAR(50) NOT NULL DEFAULT 'microservice',
  PRIMARY KEY (`id`, `username`)
  )ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
