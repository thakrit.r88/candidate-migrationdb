package services

import (
	"net/http"

	"github.com/labstack/echo"
	"github.com/spf13/viper"
)

type Servicer interface {
	Up(req *Request, filePath map[string]string) error
}

type Handler struct {
	service Servicer
}

func NewHandler(service Servicer) *Handler {
	return &Handler{
		service: service,
	}
}

var filePath map[string]string
var migrations []Migrations

func (h *Handler) UpDB(c echo.Context) error {
	var req Request
	if err := c.Bind(&req); err != nil {
		return err
	}

	initDefault()
	if req.Migrations == nil {
		req.Migrations = migrations
	}

	err := h.service.Up(&req, filePath)
	if err != nil {
		return err
	}

	return c.JSON(http.StatusOK, "Database up migrated")
}

func initDefault() {
	migrations = []Migrations{
		Migrations{
			Type: "data",
		},
		Migrations{
			Type: "schema",
		},
	}
	filePath = map[string]string{
		"schema": "file://migration/3d-secure",
		"data":   "file://migration/data/" + viper.GetString("migrationenv"),
	}

}
