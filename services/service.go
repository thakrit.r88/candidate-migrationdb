package services

import (
	"database/sql"
	"fmt"
	"sort"

	"github.com/golang-migrate/migrate/v4"
	migrateSQL "github.com/golang-migrate/migrate/v4/database/mysql"
)

type Service struct {
	db *sql.DB
}

func NewService(db *sql.DB) *Service {
	return &Service{
		db: db,
	}
}

var (
	dataMigrations = "data_migrations"

	typeSortMap = map[string]int{
		"schema": 1,
		"data":   2,
	}

	dataConfig = map[string]*migrateSQL.Config{
		"schema": &migrateSQL.Config{
			MigrationsTable: migrateSQL.DefaultMigrationsTable,
		},
		"data": &migrateSQL.Config{
			MigrationsTable: dataMigrations,
		},
	}
)

func (s *Service) Up(req *Request, filePath map[string]string) error {
	sortRequest(req)
	for _, migration := range req.Migrations {
		_type := migration.Type
		fmt.Println("Migrated Type : %s", _type)
		driver, err := migrateSQL.WithInstance(s.db, dataConfig[_type])
		if err != nil {
			fmt.Println("could not start sql migration... %v", err)
			return err
		}

		file := filePath[_type]
		fmt.Println("Migrated file path : %s", file)
		m, err := migrate.NewWithDatabaseInstance(file, "mysql", driver)
		if err != nil {
			fmt.Println("migration failed... %v", err)
			return err
		}

		forceVersion := migration.ForceVersion
		if forceVersion > 0 {
			m.Force(forceVersion)
			fmt.Println("forced to migration version %d", forceVersion)
		}

		if err := m.Up(); err != nil && err != migrate.ErrNoChange {
			fmt.Println("An error occurred while syncing the database.. %v", err)
			_, _, _ = m.Version()
			return err
		}

		fmt.Println("Database up migrated")
	}

	return nil
}

func sortRequest(req *Request) {
	sort.SliceStable(req.Migrations, func(i, j int) bool {
		typeI := req.Migrations[i].Type
		typeJ := req.Migrations[j].Type
		if typeSortMap[typeI] < typeSortMap[typeJ] {
			return true
		}
		return false
	})
}
