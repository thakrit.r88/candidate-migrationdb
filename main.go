package main

import (
	"candidate-migrationdb/services"
	"context"
	"database/sql"
	"fmt"
	"log"
	"net/http"
	"os"
	"os/signal"
	"runtime"
	"strings"
	"syscall"
	"time"

	"github.com/go-sql-driver/mysql"
	"github.com/jinzhu/gorm"
	"github.com/labstack/echo"
	"github.com/spf13/viper"
)

var (
	buildstamp string
	githash    string
)

func init() {
	runtime.GOMAXPROCS(1)
	initViper()
}

func main() {
	e := echo.New()
	maria := newMariaConn()

	defer maria.Close()
	e.GET("/health", makeHealthHandler(maria))

	dbT, err := initDB()
	if err != nil {
		fmt.Println("could not connect to the MySQL database... %v", err)
	}

	if err := dbT.Ping(); err != nil {
		fmt.Println("could not ping DB... %v", err)
	}

	ht := services.NewHandler(services.NewService(dbT))

	e.POST("/up", ht.UpDB)

	fmt.Println("Listening on port: ", viper.GetString("app.port"))
	go func() {
		fmt.Println(e.Start(":" + viper.GetString("app.port")))
	}()

	gracefulShutdown(e)
}

func initDB() (*sql.DB, error) {

	conf := mysql.Config{
		DBName:               viper.GetString("mysql.database.3d-secure"),
		User:                 viper.GetString("mysql.username"),
		Passwd:               viper.GetString("mysql.password"),
		Net:                  "tcp",
		Addr:                 viper.GetString("mysql.host") + ":" + viper.GetString("mysql.port"),
		AllowNativePasswords: true,
		Timeout:              viper.GetDuration("mysql.timeouts"),
		ReadTimeout:          viper.GetDuration("mysql.readtimeouts"),
		WriteTimeout:         viper.GetDuration("mysql.writetimeouts"),
		ParseTime:            viper.GetBool("mysql.parsetime"),
		MultiStatements:      viper.GetBool("mysql.multistatement"),
		Loc:                  time.Local,
	}
	fmt.Println("[CONFIG] db connection: ", conf.FormatDSN())

	db, err := sql.Open("mysql", conf.FormatDSN())
	if err != nil {
		return nil, err
	}

	return db, nil
}

func newMariaConn() *gorm.DB {
	conf := mysql.Config{
		DBName:               viper.GetString("mysql.database.creditcards"),
		User:                 viper.GetString("mysql.username"),
		Passwd:               viper.GetString("mysql.password"),
		Net:                  "tcp",
		Addr:                 viper.GetString("mysql.host") + ":" + viper.GetString("mysql.port"),
		AllowNativePasswords: true,
		Timeout:              viper.GetDuration("mysql.timeout"),
		ReadTimeout:          viper.GetDuration("mysql.readtimeout"),
		WriteTimeout:         viper.GetDuration("mysql.writetimeout"),
		ParseTime:            viper.GetBool("mysql.parsetime"),
		Loc:                  time.Local,
	}

	conn, err := gorm.Open("mysql", conf.FormatDSN())
	if err != nil {
		log.Fatalf("cannot open mysql connection:%s", err)
	}

	conn.DB().SetMaxIdleConns(viper.GetInt("mysql.maxidle"))
	conn.DB().SetMaxOpenConns(viper.GetInt("mysql.maxopen"))
	conn.DB().SetConnMaxLifetime(viper.GetDuration("mysql.maxlifetime"))
	conn.LogMode(viper.GetBool("mysql.debug"))

	return conn
}

func makeHealthHandler(maria *gorm.DB) echo.HandlerFunc {
	return func(c echo.Context) error {
		err := maria.DB().Ping()
		if err != nil {
			return c.JSON(http.StatusInternalServerError, map[string]string{
				"status": "unhealty",
				"msg":    fmt.Sprintf("maria ping error: %s", err.Error()),
			})
		}

		return c.JSON(http.StatusOK, map[string]string{"status": "healthy"})
	}
}

func initViper() {
	viper.AddConfigPath("./configs")
	viper.SetConfigName("config")

	if err := viper.ReadInConfig(); err != nil {
		log.Fatal("Cannot connect database with error: ", err)
	}

	viper.AutomaticEnv()
	viper.SetEnvKeyReplacer(strings.NewReplacer(".", "_"))
}

func gracefulShutdown(e *echo.Echo) {
	quit := make(chan os.Signal)
	signal.Notify(quit, os.Interrupt, syscall.SIGTERM)

	<-quit

	if err := e.Shutdown(context.Background()); err != nil {
		fmt.Println("shutdown server: %s", err)
	}
}
